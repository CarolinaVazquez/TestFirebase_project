//
//  DatePickerViewController.swift
//  TestFirebase
//
//  Created by Carolina Vazquez León on 21/10/22.
//

import Foundation
import UIKit


class DatePickerViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        // Crear el DatePicker
        let datePicker: UIDatePicker = UIDatePicker()
        
        // Posicionar el picker en la vista
        datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 200)
        
        // Agregar Propiedades del picker
        datePicker.timeZone = NSTimeZone.local
        datePicker.backgroundColor = UIColor.white
        
        // Añadir la action valueChanged
        datePicker.addTarget(self, action: #selector(DatePickerViewController.datePickerValueChanged(_:)), for: .valueChanged)
   
        // Añadir Picker a la vista
        self.view.addSubview(datePicker)
        
    }
    
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        // Crear formato
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Llamar fromato
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        // Aplicar fromato
        let selectedDate: String = dateFormatter.string(from: sender.date)
        
        print("Selected value \(selectedDate)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Nos permite deshacernos de cualquier recurso que pueda ser recreado
    }
    
}

