//
//  HomeViewController.swift
//  TestFirebase
//
//  Created by Carolina Vazquez León on 27/10/22.
//

import UIKit
import FirebaseAuth
import FirebaseAnalytics

enum ProviderType: String {
    case basic
}

class HomeViewController: UIViewController {
    
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var providerLabel: UILabel!
    @IBOutlet weak var sesionButtom: UIButton!
    
    private let email: String
    private let provider: ProviderType
    
    init(email: String, provider: ProviderType) {
        self.email = email
        self.provider = provider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) no esta implementado")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "prueba1800"
        
        emailLabel.text = email
        providerLabel.text = provider.rawValue
        
      //  Analytics.logEvent("QUIEN SABE CUAL PRUEBA CON ID",
                        //   parameters: [AnalyticsParameterContent: ""])
        
       /* Analytics.logEvent("QUIEN SABE CUAL PRUEBA CON ID", parameters: [
          AnalyticsParameterItemID: "id-\(title!)",
          AnalyticsParameterItemName: title!,
          AnalyticsParameterContentType: "cont",
        ])*/
        
        //Analytics.setDefaultEventParameters([ "Name_event": "PRUEBA_ID_DEFAULT_EVENT", "Flow_name": "domiciliacion", "ID_Flow": 5 ])
        
       // Analytics.setDefaultEventParameters("PRUEBA_DEFAULT_EVENT_CON_PARAMS_NOMNRE_EVENTO","Flow_name": "domiciliacion", "ID_Flow": 5)
        
        //START: Set_current_screen
    //    Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenClass: "HOME"])
        //END: Set_current_screen
        
   //     Analytics.logEvent(AnalyticsEventLogin, parameters: [AnalyticsEventLogin: AuthViewController.self] )
        
        // [START set_current_screen]
   //        Analytics.logEvent(AnalyticsEventScreenView,
    //                          parameters: [AnalyticsParameterScreenName: "",
   //                                        AnalyticsParameterScreenClass: ""])
           // [END set_current_screen]
    }
    

    @IBAction func closeSesionButtom(_ sender: Any) {
        
        switch provider {
            
        case .basic:
            do {
               try Auth.auth().signOut()
                navigationController?.popViewController(animated: true)
            } catch {
                // Se ha producido un error
            }
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
