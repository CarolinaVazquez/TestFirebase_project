//
//  ViewController.swift
//  TestFirebase
//
//  Created by Carolina Vazquez León on 20/10/22.
//

import UIKit
import FirebaseAnalytics
import FirebaseAuth

class AuthViewController: UIViewController {

    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    let datePicker = UIDatePicker()
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var singupButtom: UIButton!
    @IBOutlet weak var loginButtom: UIButton!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        title = "AUTENTICACIÓN"
        
        
        
   //   Analytics.logEvent("prueba_evento_con_ID", parameters: [
    //    AnalyticsParameterItemID: "TRASPASO"])
        
       Analytics.logEvent("InitScreen", parameters: ["message": "Integración de Firebase completa"])
        
        //START: Set_current_screen
     //   Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "INICIAR SESIÓN", AnalyticsParameterScreenClass: AuthViewController.self])
        //END: Set_current_screen
        
        // [START custom_event_swift]
         /*   Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
              AnalyticsParameterItemID: "id-\(title!)",
              AnalyticsParameterItemName: title!,
              AnalyticsParameterContentType: "cont",
            ])*/
            // [END custom_event_swift]
        
        Analytics.logEvent("EVENTO_PRUEBA_CON_ID_TITLE", parameters: [
           AnalyticsParameterItemID: "id-\(title!)",
           AnalyticsParameterItemName: title!,
           AnalyticsParameterContentType: "cont",
         ])
        
        createDatePicker()
        
    }
    
    
    @IBAction func singUpAction(_ sender: Any) {
        
        if let email = emailTextField.text, let password = passTextField.text {
            
            Auth.auth().createUser(withEmail: email, password: password) {
                (result, error) in
                
                if let result = result, error == nil {
                    
                    self.navigationController?
                        .pushViewController(HomeViewController(email: result.user.email!, provider: .basic), animated: true)
                    
                } else {
                    Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Error Alerta", AnalyticsParameterScreenClass: UIAlertController.self])
                    let alertController = UIAlertController(title: "Error", message: "Se ha producido un error registrando al usuario", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    @IBAction func loginAction(_ sender: Any) {
        
        if let email = emailTextField.text, let password = passTextField.text {
            
            Auth.auth().signIn(withEmail: email, password: password) {
                (result, error) in
                
                if let result = result, error == nil {
                    
                    self.navigationController?
                        .pushViewController(HomeViewController(email: result.user.email!, provider: .basic), animated: true)
                    
                } else {
                    let alertController = UIAlertController(title: "Error", message: "Se ha producido un error registrando al usuario", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func createToolbar() -> UIToolbar {
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // boton Hecho
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneBtn], animated: true)
        
        return toolbar
    }

    func createDatePicker() {
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .compact
        } else {
            // Fallback on earlier versions
            datePicker.backgroundColor = .white
        }
        datePicker.datePickerMode = .date
        
        inputTextField.textAlignment = .center
        inputTextField.inputView = datePicker
        inputTextField.inputAccessoryView = createToolbar()
    }
    
    @objc func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        
        self.inputTextField.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //Nos permite deshacernos de cualquier recurso que pueda ser recreado
    }
    
    
    

}

